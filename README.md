# viseClamps
Parametrically programmed vise clamps following the OSHWA open source hardware definition.

This vise clamp design allows you to quickly and easily fit a clamp to your machine. A solid model can be generated in OpenSCAD, if desired, before the parameters are put into the included parametric G-code. The G-code has been proven on LinuxCNC 2.7.
