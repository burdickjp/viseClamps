heightSlot = 8;
depthSlot = 8;
positionSlot = 9;
diameterClearance = 10.5;
diameterCounterbore = 16;
depthCounterbore = heightSlot -2;

lengthClamp = positionSlot + diameterClearance/2 + diameterCounterbore/2 + depthSlot;
widthClamp = diameterCounterbore +4;
heightClamp = heightSlot;

difference(){
    union(){
        translate([-widthClamp/2, - diameterClearance/2 - positionSlot, 0]){
            cube([widthClamp,lengthClamp,heightClamp]);
        }
        translate([-widthClamp/2, -diameterClearance/2 -positionSlot/2, -positionSlot/2]){
            rotate(a= 90, v=[0,1,0]){
                cylinder(h= widthClamp, d= positionSlot);
            }
        }
        translate([-widthClamp/2, -diameterClearance/2 -positionSlot, -positionSlot/2]){
            cube([widthClamp, positionSlot *1.5, positionSlot/2]);
        }
    }
    translate([0,0, -positionSlot] /2){
        cylinder(h = heightClamp + positionSlot /2, d = diameterClearance);
    }
    translate([0,0, heightClamp - depthCounterbore]){
        cylinder(h = depthCounterbore, d = diameterCounterbore);
    }
    translate([-widthClamp/2, -diameterClearance/2 + positionSlot/2, -positionSlot/2]){
        rotate(a= 90, v=[0,1,0]){
            cylinder(h= widthClamp, d= positionSlot);
        }
    }
}
